package com.example.homework5;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.homework5.Model.Category;

import java.util.List;

@Dao
public interface CategoryDao {

    @Query(" SELECT * FROM tb_category")
    List<Category> getAllCategory();

    @Insert
    void addCategory(Category... categories);

    @Query("DELETE FROM tb_category")
    void deleteCategories();

    @Query("SELECT * from tb_category where id =:id")
    Category getCategoryByID(int id);
}
