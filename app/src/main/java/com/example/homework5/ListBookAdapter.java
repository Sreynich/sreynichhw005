package com.example.homework5;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.example.homework5.Model.Book;
import com.example.homework5.Model.Category;

import java.util.List;

public class ListBookAdapter extends RecyclerView.Adapter<ListBookAdapter.ViewHolder> {

    private List<Book> bookList;
    private Context context;
    AppDatabase appDatabase;
    Book book;
    private OnClickRecyclerView onClickTest;


    public ListBookAdapter(List<Book> bookList, Context context, OnClickRecyclerView onClickRecyclerView) {
        this.bookList = bookList;
        this.context = context;
        this.onClickTest = onClickRecyclerView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listBook = layoutInflater.inflate(R.layout.list_book_layout, parent, false);


        ViewHolder viewHolder = new ViewHolder(listBook);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        book = bookList.get(position);
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "my_db")
                .allowMainThreadQueries()
                .build();
        final CategoryDao categoryDao = appDatabase.getCategoryDao();
        final Category category = categoryDao.getCategoryByID(book.getCate_id());

        holder.textTile.setText(book.getTitle());
        holder.textSize.setText(book.getSize() + " pages");
        holder.textPrice.setText("$ " + book.getPrice());
        holder.textCategory.setText(category.getName());
        final String image = book.getImageUri();
        if (image == null || image.equals(null)) {
            holder.bookImage.setImageResource(R.drawable.default_book);
        } else {

            Uri imageUri = Uri.parse(image);
            Glide.with(holder.bookImage.getContext()).load(imageUri).into(holder.bookImage);
        }

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textPrice, textSize, textTile, textCategory;
        ImageView option, bookImage;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            option = itemView.findViewById(R.id.option);
            textPrice = itemView.findViewById(R.id.textview_book_price);
            textSize = itemView.findViewById(R.id.textview_book_size);
            textTile = itemView.findViewById(R.id.textview_book_title);
            textCategory = itemView.findViewById(R.id.textview_book_category);
            bookImage = itemView.findViewById(R.id.book_layout_image);
            option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Book book = bookList.get(getAdapterPosition());
                    onClickTest.onItemClick(v, getAdapterPosition(), book);
                }
            });

        }
    }


}
