package com.example.homework5;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.homework5.Model.Book;
import com.example.homework5.Model.Category;

@Database(entities = {Book.class, Category.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract BookDao getBookDao();
    public abstract CategoryDao getCategoryDao();
}
