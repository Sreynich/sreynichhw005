package com.example.homework5;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.homework5.Model.Book;

import java.util.List;

@Dao
public interface BookDao {

    @Insert
    void addBook(Book book);

    @Query("SELECT * FROM tb_book")
    List<Book> getAllBooks();

    @Query("SELECT * FROM tb_book where id =:id")
    Book getBookByID(int id);

    @Update
    void updateBook(Book book);

    @Query("SELECT * from tb_book where title LIKE '%'|| :title  || '%'")
    List<Book> getBooksByTitle(String title);

    @Query("Delete from tb_book where id =:id")
    void removeBookById(int id);

    @Query("Delete from tb_book")
    void deleteBook();


}
