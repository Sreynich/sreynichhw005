package com.example.homework5;

import android.view.View;

import com.example.homework5.Model.Book;

public interface OnClickRecyclerView {
    void onItemClick(View view, int position, Book book);
}
