package com.example.homework5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.example.homework5.Model.Book;
import com.example.homework5.Model.Category;
import com.example.homework5.dialogs.AddDialogFragmentActivity;
import com.example.homework5.dialogs.UpdateDialogFragment;
import com.example.homework5.dialogs.ViewDialogFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DialogListener, OnClickRecyclerView {

    Button buttonAdd, btnSearch;
    EditText editTextSearch;
    AppDatabase appDatabase;
    RecyclerView recyclerView;
    List<Book> listBooks = new ArrayList<>();
    ListBookAdapter adapter;
    ImageView option;
    List<Book> searchedBook = new ArrayList<>();
    List<Category> categories = new ArrayList<>();
    BottomNavigationView navigationView;

    String searchTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "my_db")
                .allowMainThreadQueries()
                .build();
        if (appDatabase.getCategoryDao().getAllCategory() == null || appDatabase.getCategoryDao().getAllCategory().isEmpty()) {
            addCategory();
        }
        if (appDatabase.getBookDao().getAllBooks() != null) {
            showBook();
        }


        buttonAdd = findViewById(R.id.button_add);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBook();
            }
        });

        btnSearch = findViewById(R.id.button_search);
        editTextSearch = findViewById(R.id.editText_search);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.setFocusable(false);
                searchTitle = editTextSearch.getText().toString();
                searchBooks(searchTitle);
            }
        });
        navigationView = findViewById(R.id.bottom_navi);

        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home:
                        editTextSearch.setText("");
                        editTextSearch.setFocusableInTouchMode(true);
                        editTextSearch.setFocusable(true);
                        showBook();
                        return true;
                }
                return true;
            }
        });

    }

    public void addBook() {
        AddDialogFragmentActivity customAddDialogActivity = new AddDialogFragmentActivity(getApplicationContext());
        customAddDialogActivity.setCancelable(false);
        customAddDialogActivity.show(getSupportFragmentManager(), "add dialog");
    }

    @Override
    public void addData(String title, int cate_id, int size, float price, String uri) {
        appDatabase.getBookDao().addBook(new Book(title, cate_id, size, price, uri));

        showBook();
    }

    @Override
    public void updateBook(int id, String nTitle, int nCate_id, int nSize, float nPrice, String nUri) {
        Book book = new Book();
        book.setId(id);
        book.setCate_id(nCate_id);
        book.setTitle(nTitle);
        book.setSize(nSize);
        book.setPrice(nPrice);
        book.setImageUri(nUri);
        appDatabase.getBookDao().updateBook(book);
        showBook();
    }


    public void showBook() {


        recyclerView = findViewById(R.id.recyclerview);
        listBooks = appDatabase.getBookDao().getAllBooks();
        adapter = new ListBookAdapter(listBooks, getApplicationContext(), this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
    }

    public void addCategory() {

        appDatabase.getCategoryDao().deleteCategories();
        appDatabase.getCategoryDao().addCategory(new Category("Donate"));
        appDatabase.getCategoryDao().addCategory(new Category("Sale"));

        categories = appDatabase.getCategoryDao().getAllCategory();


    }

    public void clearBooks() {
        appDatabase.getBookDao().deleteBook();
    }

    public void removeBook(int id) {
        appDatabase.getBookDao().removeBookById(id);


    }

    public void searchBooks(String title) {
        navigationView.getMenu().getItem(0).setChecked(true);
        searchedBook = appDatabase.getBookDao().getBooksByTitle(title);
        adapter = new ListBookAdapter(searchedBook, getApplicationContext(), this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onItemClick(View view, final int position, final Book book) {

        option = view.findViewById(R.id.option);

        option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), option);
                popupMenu.getMenuInflater().inflate(R.menu.option_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case (R.id.view_item):
                                ViewDialogFragment viewDialogFragment = new ViewDialogFragment(getApplicationContext(), book);
                                viewDialogFragment.show(getSupportFragmentManager(), "view dialog");
                                return true;
                            case (R.id.edit_item):
                                UpdateDialogFragment updateDialogFragment = new UpdateDialogFragment(book, getApplicationContext());
                                updateDialogFragment.setCancelable(false);
                                updateDialogFragment.show(getSupportFragmentManager(), "update dialog");

                                return true;
                            case (R.id.remove_item):

                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setTitle("Warning !");
                                builder.setMessage("Do you want to delete it?");
                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        removeBook(book.getId());
                                        showBook();
                                    }
                                });
                                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                builder.show();

                                return true;

                        }

                        return true;
                    }
                });
                popupMenu.show();
            }
        });


    }


}
