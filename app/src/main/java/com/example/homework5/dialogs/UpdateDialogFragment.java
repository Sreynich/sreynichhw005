package com.example.homework5.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.example.homework5.AppDatabase;
import com.example.homework5.DialogListener;
import com.example.homework5.Model.Book;
import com.example.homework5.Model.Category;
import com.example.homework5.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class UpdateDialogFragment extends DialogFragment implements EasyPermissions.PermissionCallbacks {

    View view;
    Book book;
    Context context;
    Spinner spinner;
    AppDatabase appDatabase;
    List<Category> categories;
    ImageView imageView;
    FloatingActionButton floatingActionButton;
    EditText mTitle, mPrice, mSize;
    DialogListener dialogListener;
    String imageUriString;
    Uri imageUir;

    public UpdateDialogFragment(Book book, Context context) {
        this.book = book;
        this.context = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_layout, null);


        appDatabase = Room.databaseBuilder(getContext(), AppDatabase.class, "my_db")
                .allowMainThreadQueries()
                .build();

        categories = appDatabase.getCategoryDao().getAllCategory();

        spinner = view.findViewById(R.id.category_spinner);
        ArrayAdapter<Category> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        imageView = view.findViewById(R.id.book_image);
        floatingActionButton = view.findViewById(R.id.floating_button);
        mTitle = view.findViewById(R.id.edit_title);
        mPrice = view.findViewById(R.id.edit_price);
        mSize = view.findViewById(R.id.edit_size);

        mTitle.setText(book.getTitle());
        mPrice.setText("" + book.getPrice());
        mSize.setText("" + book.getSize());
        final String image = book.getImageUri();
        if (image == null || image.equals(null)) {
            imageView.setImageResource(R.drawable.default_book);
        } else {

            Uri imageUri = Uri.parse(image);
            Glide.with(imageView.getContext()).load(imageUri).into(imageView);
        }

        builder.setTitle("Update Book");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String title = mTitle.getText().toString();
                Category category = (Category) spinner.getSelectedItem();
                String s = mSize.getText().toString();
                String p = mPrice.getText().toString();

                if (imageUriString == null || imageUriString.equals(null) || s.matches("") || p.matches("") || title.matches("")) {
                    imageUriString = book.getImageUri();
                    Toast.makeText(context, " Invalid input! Updating is failed", Toast.LENGTH_LONG).show();
                } else {

                    int size = Integer.parseInt(s);
                    float price = Float.parseFloat(p);
                    dialogListener.updateBook(book.getId(), title, category.getId(), size, price, imageUriString);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadPicture();
            }
        });
        builder.setView(view);
        return builder.create();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            dialogListener = (DialogListener) getContext();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddDialogListener");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        chooseImage();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Toast.makeText(context, "permission denied", Toast.LENGTH_LONG).show();
    }

    @AfterPermissionGranted(100)
    private void uploadPicture() {

        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(context, permissions)) {
            chooseImage();
        } else {
            EasyPermissions.requestPermissions(this, "We need the permission", 100, permissions);
        }
    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1000);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {


            imageUir = data.getData();
            imageUriString = imageUir.toString();
            try {
                InputStream inputStream = context.getContentResolver().openInputStream(imageUir);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }

}
