package com.example.homework5.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.example.homework5.AppDatabase;
import com.example.homework5.Model.Book;
import com.example.homework5.Model.Category;
import com.example.homework5.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class ViewDialogFragment extends DialogFragment {

    View view;
    Context context;
    Book book;
    AppDatabase appDatabase;
    List<Category> categories;
    Spinner spinner;
    ImageView imageView;
    FloatingActionButton floatingActionButton;
    EditText mTitle, mPrice, mSize;

    public ViewDialogFragment(Context context, Book book) {
        this.context = context;
        this.book = book;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_layout, null);
        appDatabase = Room.databaseBuilder(getContext(), AppDatabase.class, "my_db")
                .allowMainThreadQueries()
                .build();

        categories = appDatabase.getCategoryDao().getAllCategory();
        spinner = view.findViewById(R.id.category_spinner);
        ArrayAdapter<Category> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        imageView = view.findViewById(R.id.book_image);
        floatingActionButton = view.findViewById(R.id.floating_button);
        mTitle = view.findViewById(R.id.edit_title);
        mPrice = view.findViewById(R.id.edit_price);
        mSize = view.findViewById(R.id.edit_size);

        mTitle.setText(book.getTitle());
        mPrice.setText("" + book.getPrice());
        mSize.setText("" + book.getSize());
        final String image = book.getImageUri();
        if (image == null || image.equals(null)) {
            imageView.setImageResource(R.drawable.default_book);
        } else {

            Uri imageUri = Uri.parse(image);
            Glide.with(imageView.getContext()).load(imageUri).into(imageView);
        }
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setView(view);
        return builder.create();
    }
}
