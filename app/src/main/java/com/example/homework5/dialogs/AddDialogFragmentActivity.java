package com.example.homework5.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.room.Room;

import com.example.homework5.AppDatabase;
import com.example.homework5.DialogListener;
import com.example.homework5.MainActivity;
import com.example.homework5.Model.Category;
import com.example.homework5.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class AddDialogFragmentActivity extends DialogFragment implements EasyPermissions.PermissionCallbacks {

    Spinner spinner;
    List<Category> categories;
    View view;
    DialogListener dialogListener;
    EditText mTitle, mSize, mPrice;
    AppDatabase appDatabase;
    FloatingActionButton floatingActionButton;
    Uri imageUir;
    Context context;
    ImageView imageView;
    String imageUriString;


    public AddDialogFragmentActivity(Context context) {

        this.context = context;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_layout, null);

        spinner = view.findViewById(R.id.category_spinner);


        appDatabase = Room.databaseBuilder(getContext(), AppDatabase.class, "my_db")
                .allowMainThreadQueries()
                .build();
        categories = appDatabase.getCategoryDao().getAllCategory();


        ArrayAdapter<Category> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        imageView = view.findViewById(R.id.book_image);
        floatingActionButton = view.findViewById(R.id.floating_button);
        mTitle = view.findViewById(R.id.edit_title);
        mPrice = view.findViewById(R.id.edit_price);
        mSize = view.findViewById(R.id.edit_size);

        builder.setTitle("Add New Book");

        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addData();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadPicture();
            }
        });


        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            dialogListener = (DialogListener) getContext();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddDialogListener");
        }
    }

    private void addData() {

        final Category category = (Category) spinner.getSelectedItem();

        String title = mTitle.getText().toString();
        String size = mSize.getText().toString();
        String price = mPrice.getText().toString();

        if (title.matches("") || size.matches("") || price.matches("")) {
            Toast.makeText(context, "Invalid Input! Adding data is failed", Toast.LENGTH_LONG).show();
        } else {

            int s = Integer.parseInt(mSize.getText().toString());
            float p = Float.parseFloat(mPrice.getText().toString());
            dialogListener.addData(title, category.getId(), s, p, imageUriString);
        }


    }

    @AfterPermissionGranted(123)
    private void uploadPicture() {

        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(context, permissions)) {
            chooseImage();
        } else {
            EasyPermissions.requestPermissions(this, "We need the permission", 123, permissions);
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        chooseImage();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Toast.makeText(context, "permission denied", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {

            imageUir = data.getData();
            imageUriString = imageUir.toString();
            try {
                InputStream inputStream = context.getContentResolver().openInputStream(imageUir);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }
}
