package com.example.homework5;


public interface DialogListener {
    void addData(String title, int cate_id, int size, float price, String uri);

    void updateBook(int id, String nTitle, int nCate_id, int nSize, float nPrice, String nUri);
}