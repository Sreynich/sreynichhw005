package com.example.homework5.Model;

import android.net.Uri;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_book")
public class Book {
    @PrimaryKey(autoGenerate = true)
    int id;
    String title;
    int cate_id;
    int size;
    float price;
    String imageUri;

    public Book(){}
    public Book(String title, int cate_id, int size, float price, String imageUri) {
        this.title = title;
        this.cate_id = cate_id;
        this.size = size;
        this.price = price;
        this.imageUri = imageUri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCate_id() {
        return cate_id;
    }

    public void setCate_id(int cate_id) {
        this.cate_id = cate_id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", cate_id=" + cate_id +
                ", size=" + size +
                ", price=" + price +
                ", imageUri='" + imageUri + '\'' +
                '}';
    }
}
